'use strict';

const fs = require('fs');
const path = require('path');
const router = require('koa-router')();

function getFiles (){
    let files =fs.readdirSync(__dirname);

    let js_files = files.filter(item=>{
        return item.endsWith('.js') && item !== 'index.js';
    })

    return js_files;
}

function registerRouter(files){
    files.forEach(element => {
        let filePath = path.join(__dirname,element);
        let routerPath = require(filePath);

        for(var item in routerPath){
            let type = routerPath[item][0];
            let fn = routerPath[item][1];

            if(type === 'get'){
                router.get(item,fn)
            }else if(type === 'post'){
                router.post(item,fn);
            }
        }
    });
}

module.exports = function(){
    let files = getFiles();
    registerRouter(files);
    return router.routes();
}